package net.customware.jira.plugins.ext.mercurial;

public class WebLinkType extends Object
{
    private final String key;
    private final String name;
    private final ViewLinkFormat viewLinkFormat;

    public WebLinkType(String key, String name, String changeset, String fileModified)
    {
        this.key = key;
        this.name = name;
        viewLinkFormat = new ViewLinkFormat(key, changeset, fileModified);
    }

    public String getKey()
    {
        return key;
    }

    public String getName()
    {
        return name;
    }

    public String getChangesetFormat()
    {
        return viewLinkFormat.getChangesetFormat();
    }

    public String getFileModifiedFormat()
    {
        return viewLinkFormat.getFileModifiedFormat();
    }
}
