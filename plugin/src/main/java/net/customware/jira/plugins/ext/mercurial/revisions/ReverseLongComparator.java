package net.customware.jira.plugins.ext.mercurial.revisions;

import java.util.Comparator;

public class ReverseLongComparator implements Comparator<Long>
{
    public static final Comparator<Long> COMPARATOR = new ReverseLongComparator();

    private ReverseLongComparator()
    {
    // use static instance
    }

    public int compare(final Long o1, final Long o2)
    {
        if (o1 == o2)
        {
            return 0;
        }
        if (o1 == null)
        {
            return -1; //null is smaller than anything
        }
        if (o2 == null)
        {
            return 1;
        }
        return -1*(o1).compareTo(o2);
    }
}
