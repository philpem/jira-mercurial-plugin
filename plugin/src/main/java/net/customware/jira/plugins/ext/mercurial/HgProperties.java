package net.customware.jira.plugins.ext.mercurial;

import com.opensymphony.module.propertyset.PropertySet;

public interface HgProperties {
    String getRoot();
    
    String getClonedir();
    
    String getSubreposcmd();
    
    String getDisplayName();
    
    String getReleasenotesemail();
    
    String getReleasenoteslatest();
    
    String getUsername();
    
    String getPassword();
    
    Boolean getSubrepos();
    
    Boolean getRevisionIndexing();
    
    Integer getRevisionCacheSize();
    
    String getPrivateKeyFile();
    
    String getWebLinkType();
    
    String getChangesetFormat();
    
    String getFileModifiedFormat();
    
    // Note that nothing about the active/inactive status is stored since
    // we want to try to make all the repositories active after restart
    // TODO is that actually what is wanted?

    static class Util {
        static PropertySet fillPropertySet(HgProperties properties, PropertySet propertySet) {
            propertySet.setString(MultipleMercurialRepositoryManager.HG_ROOT_KEY, properties.getRoot());
            propertySet.setString(MultipleMercurialRepositoryManager.HG_CLONEDIR_KEY, properties.getClonedir());
            propertySet.setString(MultipleMercurialRepositoryManager.HG_RELEASENOTESEMAIL_KEY, properties.getReleasenotesemail());
            propertySet.setString(MultipleMercurialRepositoryManager.HG_RELEASENOTESLATEST_KEY, properties.getReleasenoteslatest());
            propertySet.setString(MultipleMercurialRepositoryManager.HG_SUBREPOSCMD_KEY, properties.getSubreposcmd());
            propertySet.setString(MultipleMercurialRepositoryManager.HG_REPOSITORY_NAME, properties.getDisplayName() != null ? properties.getDisplayName() : properties.getRoot());
            propertySet.setString(MultipleMercurialRepositoryManager.HG_USERNAME_KEY, properties.getUsername());
            propertySet.setString(MultipleMercurialRepositoryManager.HG_PASSWORD_KEY, MercurialManagerImpl.encryptPassword(properties.getPassword()));
            propertySet.setBoolean(MultipleMercurialRepositoryManager.HG_REVISION_INDEXING_KEY, properties.getRevisionIndexing().booleanValue());
            propertySet.setBoolean(MultipleMercurialRepositoryManager.HG_SUBREPOS_KEY, properties.getSubrepos().booleanValue());
            propertySet.setInt(MultipleMercurialRepositoryManager.HG_REVISION_CACHE_SIZE_KEY, properties.getRevisionCacheSize().intValue());
            propertySet.setString(MultipleMercurialRepositoryManager.HG_PRIVATE_KEY_FILE, properties.getPrivateKeyFile());
            propertySet.setString(MultipleMercurialRepositoryManager.HG_LINKFORMAT_TYPE, properties.getWebLinkType());
            propertySet.setString(MultipleMercurialRepositoryManager.HG_LINKFORMAT_CHANGESET, properties.getChangesetFormat());
            propertySet.setString(MultipleMercurialRepositoryManager.HG_LINKFORMAT_FILE_MODIFIED, properties.getFileModifiedFormat());
            return propertySet;
        }

        static MercurialProperties fillHgProperties(PropertySet propertySet, MercurialProperties properties) {
            properties.setRoot(propertySet.getString(MultipleMercurialRepositoryManager.HG_ROOT_KEY));
            properties.setClonedir(propertySet.getString(MultipleMercurialRepositoryManager.HG_CLONEDIR_KEY));
            properties.setReleasenotesemail(propertySet.getString(MultipleMercurialRepositoryManager.HG_RELEASENOTESEMAIL_KEY));
            properties.setReleasenoteslatest(propertySet.getString(MultipleMercurialRepositoryManager.HG_RELEASENOTESLATEST_KEY));
            properties.setSubreposcmd(propertySet.getString(MultipleMercurialRepositoryManager.HG_SUBREPOSCMD_KEY));
            // TODO is displayName always set?
            properties.setDisplayName(propertySet.getString(MultipleMercurialRepositoryManager.HG_REPOSITORY_NAME));
            properties.setUsername(propertySet.getString(MultipleMercurialRepositoryManager.HG_USERNAME_KEY));
            properties.setUsername(propertySet.getString(MultipleMercurialRepositoryManager.HG_USERNAME_KEY));
            properties.setPassword(propertySet.getString(MultipleMercurialRepositoryManager.HG_PASSWORD_KEY));
            properties.setRevisionIndexing(propertySet.getBoolean(MultipleMercurialRepositoryManager.HG_REVISION_INDEXING_KEY));
            properties.setSubrepos(propertySet.getBoolean(MultipleMercurialRepositoryManager.HG_SUBREPOS_KEY) == true);
            // sic
            properties.setRevisioningCacheSize(propertySet.getInt(MultipleMercurialRepositoryManager.HG_REVISION_CACHE_SIZE_KEY));
            properties.setPrivateKeyFile(propertySet.getString(MultipleMercurialRepositoryManager.HG_PRIVATE_KEY_FILE));
            properties.setWebLinkType(propertySet.getString(MultipleMercurialRepositoryManager.HG_LINKFORMAT_TYPE));
            // sic
            properties.setChangeSetFormat(propertySet.getString(MultipleMercurialRepositoryManager.HG_LINKFORMAT_CHANGESET));
            properties.setFileModifiedFormat(propertySet.getString(MultipleMercurialRepositoryManager.HG_LINKFORMAT_FILE_MODIFIED));
            return properties;
        }
    }
}
