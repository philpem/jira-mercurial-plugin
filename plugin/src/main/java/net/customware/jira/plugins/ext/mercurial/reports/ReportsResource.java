package net.customware.jira.plugins.ext.mercurial.reports;

import net.customware.hg.core.io.HGLogEntry;
import net.customware.jira.plugins.ext.mercurial.*; 
import net.customware.jira.plugins.ext.mercurial.revisions.*;

import org.ofbiz.core.entity.GenericValue;

import com.atlassian.core.util.map.EasyMap;
import com.atlassian.core.util.collection.EasyList;
import com.atlassian.crowd.embedded.api.User;
import com.atlassian.jira.util.JiraKeyUtils;
import com.atlassian.jira.ofbiz.OfBizDelegator;
import com.atlassian.jira.ofbiz.DefaultOfBizDelegator;
import com.atlassian.core.ofbiz.CoreFactory;
import com.atlassian.jira.ComponentManager;
import com.atlassian.jira.ManagerFactory;
import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.CustomFieldManager;
import com.atlassian.jira.issue.IssueManager;
import com.atlassian.jira.issue.index.IndexException;
import com.atlassian.jira.project.ProjectManager;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.user.util.UserUtil;
import com.atlassian.jira.util.ErrorCollection;
import com.atlassian.jira.web.bean.I18nBean;
import com.atlassian.jira.util.SimpleErrorCollection;
import com.atlassian.jira.util.velocity.DefaultVelocityRequestContextFactory;
import com.atlassian.jira.util.velocity.VelocityRequestContext;
import com.atlassian.mail.Email;
import com.atlassian.mail.MailFactory;
import com.atlassian.mail.MailException;
import com.atlassian.mail.server.MailServerManager;
import com.atlassian.mail.server.SMTPMailServer;
import com.atlassian.plugins.rest.common.security.AnonymousAllowed;
import com.atlassian.sal.api.user.UserManager;
import com.atlassian.velocity.VelocityManager;

import java.util.*;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.IOException;

import org.apache.velocity.exception.VelocityException;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import static com.atlassian.jira.rest.v1.util.CacheControl.NO_CACHE;

/**
 */
@Path("/reports")
public class ReportsResource
{
    private static final Log log = LogFactory.getLog(ReportsResource.class);

    private final ApplicationProperties applicationProperties;
    private IssueManager issueManager;
    private CustomFieldManager customFieldManager;
    private final MailServerManager mailServerManager;
    private final OfBizDelegator ofBizDelegator;
    private PermissionManager permissionManager;
    private ProjectManager projectManager;
    private UserManager userManager;
    private UserUtil userUtil;
    private final VelocityManager velocityManager;
    private VelocityRequestContext velocityRequestContext;

    public final static String DEDUCE = "DEDUCE";
    public final static String PREVIOUS = "PREVIOUS";
    public final static String LATEST = "LATEST";

    private MultipleMercurialRepositoryManager multipleMercurialRepositoryManager;

    /**
     */
    public ReportsResource(UserManager userManager)
    {
        // The userManager can be null when called from a service

        this.userManager = userManager;

        // Avoid DI
        this.ofBizDelegator = new DefaultOfBizDelegator(CoreFactory.getGenericDelegator());
        this.applicationProperties = ManagerFactory.getApplicationProperties();
        this.customFieldManager = ManagerFactory.getCustomFieldManager();
        this.issueManager = ManagerFactory.getIssueManager();
        this.mailServerManager = ComponentManager.getInstance().getMailServerManager();
        this.permissionManager = ManagerFactory.getPermissionManager();
        this.projectManager = ManagerFactory.getProjectManager();
        this.userUtil = ComponentManager.getInstance().getUserUtil();
        this.velocityManager = ManagerFactory.getVelocityManager();


        // See JiraVelocityUtils and 
        // com/atlassian/jira/ext/charting/gadgets/charts/WorkloadPieChart.java
        // and also the labels plugin VelocityUtils class 
        velocityRequestContext = new DefaultVelocityRequestContextFactory(applicationProperties).getJiraVelocityRequestContext();

        multipleMercurialRepositoryManager = RevisionIndexService.getMultipleMercurialRepositoryManager();
    }

    /**
     * The REST resource for generating release notes. 
     * 
     * Required parameters:
     *
     * @param repo The name of the Mercurial repository as configured
     * in JIRA, e.g. wts
     *
     * Optional parameters:
     *
     * @param artifact The artifact id used in releases,
     * e.g. wts. Default: the repository name in JIRA
     *
     * @param start The revision to start the release notes
     * from. Default: "PREVIOUS", which will look for the previous
     * release. Mercurial tags are also supported.
     *
     * @param end The revision to end the release notes at. Default:
     * "LATEST", which will look for the latest release. Mercurial
     * tags are also supported.
     *
     * @param titleprefix Text that is prepended to the title of the
     * release notes report
     *
     * @param email A comma-separated list of email addresses that
     * should receive the release notes HTML.
     *
     * @param addressedinfieldname The name of a JIRA text field to be
     * updated with the release name. Default is to not do this.
     */
    @GET
    @Path("/release")
    @AnonymousAllowed
    @Produces({MediaType.TEXT_HTML})
    public Response getReleaseNotes(@Context HttpServletRequest request,
                                    @QueryParam ("repo") String repoName,
                                    @DefaultValue(DEDUCE) @QueryParam ("artifact") String artifactId,
                                    @DefaultValue(PREVIOUS) @QueryParam ("start") String startChangeSet,
                                    @DefaultValue(LATEST) @QueryParam ("end") String endChangeSet,
                                    @DefaultValue("") @QueryParam ("titleprefix") String titlePrefix,
                                    @DefaultValue("none") @QueryParam ("email") String emailAddresses,
                                    @DefaultValue("") @QueryParam ("addressedinfieldname") String addressedInFieldName)
    {
        // the request was automatically injected with @Context, so
        // we can use SAL to extract the username from it
        String username = userManager.getRemoteUsername(request);

        // get the corresponding com.opensymphony.os.User object for
        // the request. The user changes the i18n of the report html.
        User user = userUtil.getUser(username);

        final ErrorCollection errors = new SimpleErrorCollection();

        // TODO unescape addressedInFieldName from a URL parameter?
        String html = getReleaseNotesHtml(true, errors, startChangeSet, endChangeSet, artifactId, repoName, titlePrefix, emailAddresses, user, addressedInFieldName);
        if (errors.hasAnyErrors()) {
            // BAD_REQUEST is not supported for ErrorCollection
            //final com.atlassian.jira.rest.v1.model.errors.ErrorCollection errorCollection = newBuilder().addErrorCollection(errors).build();
            return Response.status(Response.Status.BAD_REQUEST).entity(displayMessages(errors)).cacheControl(NO_CACHE).build();
        }

        // Also return the HTML report
        return Response.ok(html).cacheControl(NO_CACHE).build();

    }

    /**
     * @return a String from the errors suitable for use as HTML
     */
    private String displayMessages(ErrorCollection errors) {
        StringBuffer sb = new StringBuffer();
        if (!errors.getErrorMessages().isEmpty() || 
            !errors.getErrors().isEmpty()) {
            // TODO i18n
            sb.append("<h3>Error</h3>");
        }
        for (Iterator it=errors.getErrorMessages().iterator(); it.hasNext(); ) {
            String msg = (String)it.next();
            if (msg != null && !msg.equals("")) {
                sb.append(msg + "<br>");
            }
        }
        for (Iterator it=errors.getErrors().entrySet().iterator(); it.hasNext(); ) {
            Map.Entry entry = (Map.Entry)it.next();
            String fieldName = (String)entry.getKey();
            String msg = (String)entry.getValue();
            if (msg != null && !msg.equals("")) {
                sb.append(fieldName + ":" + msg + "<br>");
            }
        }
        return sb.toString();
    }

    /**
     * This method is called via the REST resource and also from the
     * JIRA service.
     */
    public String getReleaseNotesHtml(boolean isManual, 
                                      ErrorCollection errors, 
                                      String startChangeSet, 
                                      String endChangeSet, 
                                      String artifactId, 
                                      String repoName, 
                                      String titlePrefix, 
                                      String emailAddresses, 
                                      User user, 
                                      String addressedInFieldName) {
        MercurialManager manager = multipleMercurialRepositoryManager.getRepository(repoName);
        if (manager == null) {
            String msg = "No Mercurial repository with display name '" + repoName + "' found";
            log.warn(msg);
            errors.addErrorMessage(msg);
            return null;
        }

        List<String> warnings = new ArrayList<String>();
        Map otherParams = new HashMap();

        Set<String> issueKeys = getIssueKeys(isManual, errors, warnings, startChangeSet, endChangeSet, artifactId, manager, otherParams);
        if (errors.hasAnyErrors()) {
            return null;
        }
        artifactId = (String)otherParams.get("DEDUCEDARTIFACTID");
        
        // Make sure that the JIRA service doesn't email the same release notes
        // or update each issues' field more than once. Manually
        // generated reports are not stopped from doing that.
        String changedVersion = null;
        if (!isManual) {
            String versionName = (String)otherParams.get("versionName");
            if (versionName == null) {
                log.debug("No release name was found automatically so not sending release notes email or updating a release field");
                return null;
            }
            String lastVersionName = (String)otherParams.get("lastVersionName");
            if (lastVersionName == null) {
                // This happens this first time that this is run in a
                // new JIRA instance
                lastVersionName = versionName;
                String msg = "The version name is set to " + versionName + " but the last version name is not set. Using the version name.";
                log.debug(msg);
            }
            if (lastVersionName.equalsIgnoreCase(versionName)) {
                log.warn("The latest release automatically detected is unchanged: " + versionName);
                return null;
            }
            // A new version has been automatically found
            changedVersion = versionName;
            log.warn("Automatically found a new release: " + changedVersion);
        }
        // TODO manual execution uses individual checks in methods called from 
        // here but could we just return at this point?

        if (addressedInFieldName != null && !addressedInFieldName.equals("")) {
            updateReleaseField(errors, warnings, addressedInFieldName, issueKeys, otherParams);
            if (errors.hasAnyErrors()) {
                return null;
            }
        } else {
            log.debug("addressedInFieldName is not set so no issues will be updated");
        }
        
        String html = getReport(errors, warnings, user, issueKeys, repoName, titlePrefix, otherParams);
        if (errors.hasAnyErrors()) {
            log.debug("No email sent due to errors in generating the HTML for " + repoName);
            return null;
        }

        // TODO could use a template for the email subject using i18n release.notes.heading
        // String subject = titlePrefix + "Release Notes - " + repoName + " - " + otherParams.get("versionName");
        String subject = repoName + " Release Notes - " + otherParams.get("versionName");
        sendEmail(emailAddresses, html, subject);
        
        // If this was run by a service, we finally update the stored
        // release so we don't spam everyone the next time the
        // service runs.
        if (!isManual && changedVersion != null) {
            manager.recordRelease(artifactId, changedVersion);
        }

        return html;
    }

    /**
     * Split the parameter into email addresses and mail the HTML report
     */
    protected void sendEmail(String emailAddresses, String html, String subject) {
        if (emailAddresses.equals("none")) {
            return;
        }
        String[] addresses = ((String)emailAddresses).split(",");
        Set<String> cleaned = new HashSet<String>();
        for (int i=0; i<addresses.length; i++) {
            String email = addresses[i].trim();
            if (email.indexOf("@") == -1) {
                log.warn("Email not sent due bad address format: " + email);
            } else {
                cleaned.add(email);
            }
        }
        
        SMTPMailServer server;
        try {
            server = mailServerManager.getDefaultSMTPMailServer();
        } catch (Exception me) {
            log.warn(me.getMessage());
            return;
        }
        if (server == null || MailFactory.isSendingDisabled()) {
            log.warn("No mail server is configured or enabled for sending release notes as email");
            return;
        }

        // TODO this assumes that the user has their preference set to HTML?
        String mimeType = "text/html";
        for (String addr: cleaned) {
            Email email = new Email(addr);
            email.setFrom(server.getDefaultFrom());
            // TODO email.setReplyTo();
            // TODO email.setMultipart(javax.mail.Multipart);
            email.setSubject(subject);
            email.setMimeType(mimeType);
            email.setBody(html);
            
            try {
                // NOTE: The message is sent directly, i.e. is NOT queued
                // MailQueueItems are more complex than their worth for this

                server.send(email);
                /*
                String oldPrefix = server.getPrefix();
                if (oldPrefix == null || oldPrefix.equals("")) {
                    // No prefix that we care about has been set
                    server.send(email);
                } else {
                    // If the server object is a singleton, there is a
                    // small concurrency risk with the prefix change.
                    server.setPrefix("");
                    server.send(email);
                    server.setPrefix(oldPrefix);
                }
                */

            } catch (MailException me) {
                log.warn("Unable to send release notes email to " + addr + ":" + me.getMessage());
            }
        }
    }

    /**
     * Get the set of issue keys referred to in commit messages
     * between the given changesets.
     *
     * Populate warnings as necessary
     *
     * Note: side effect: sets versionName, artifactId etc in otherParams
     */
    protected Set<String> getIssueKeys(boolean isManual,
                                       ErrorCollection errors,
                                       List<String> warnings, 
                                       String startChangeSet, 
                                       String endChangeSet, 
                                       String artifactId,
                                       MercurialManager manager,
                                       Map otherParams) {
        String repoName = manager.getDisplayName();
        
        RevisionIndexer indexer = multipleMercurialRepositoryManager.getRevisionIndexer();

        // TODO add support for searching for the release by name or date?
        // TODO generalize for more patterns
        ReleaseDetector releaseDetector = null;
        try {
            releaseDetector = new MavenReleasePatternDetector(manager, indexer, artifactId);
            otherParams.put("DEDUCEDARTIFACTID", releaseDetector.getLatestArtifactId());
        } catch (Exception ex) {
            log.warn("Unable to create a release detector, so numeric changeset identifiers are the only choice currently enabled: " + ex.getMessage());
            otherParams.put("DEDUCEDARTIFACTID", manager.getDisplayName());
        }

        // This does an hg pull to get the latest changesets before
        // updating the index
        if (isManual) {
            try {
                indexer.checkAndUpdateOneIndex(manager);
            } catch (ConcurrentModificationException e) {
                log.info("Not indexing during clone or pull operations");
            } catch (IOException e) {
                log.warn("Unable to index repository '" + manager.getDisplayName() + "'", e);
            } catch (IndexException e) {
                log.warn("Unable to index repository '" + manager.getDisplayName() + "'", e);
            } catch (RuntimeException e) {
                log.warn("Unable to index repository '" + manager.getDisplayName() + "'", e);
            }
        }
        long latest = indexer.getLatestIndexedRevision(manager.getId());

        long start = getStartValue(errors, warnings, startChangeSet, releaseDetector, manager, indexer, otherParams);
        if (errors.hasAnyErrors()) {
            return null;
        }

        long end = getEndValue(errors, warnings, endChangeSet, releaseDetector, manager, indexer, otherParams, latest);
        if (errors.hasAnyErrors()) {
            return null;
        }
        
        // Turn the start and end revisions into links if possible
        // TODO could do this for issue keys in warnings too
        if (manager.getLinkRenderer() != null) {
            if (start != -1) {
                otherParams.put("start", manager.getLinkRenderer().getRevisionLink(start));
            }
            if (end != -1) {
                otherParams.put("end", manager.getLinkRenderer().getRevisionLink(end));
            }
        }

        // Find all the issues refered to by the changesets
        Set<String> allIssueKeys = new HashSet<String>();

        // Prime the cache
        manager.getLogEntry(end);

        // TODO could use a method with a range of revisions instead
        // for efficiency. See getLogEntry() for comments and cache
        // info
        for (long revision=start; revision<=end; revision++) {
            Set<String> issueKeys;
            try {
                issueKeys = indexer.getIssueKeysByRevision(manager, revision);
            } catch (Exception ex) {
                log.warn("Unable to get issues for revision " + revision + ", message: " + ex.getMessage(), ex);
                continue;
            }

            if (issueKeys == null || issueKeys.isEmpty()) {
                HGLogEntry logEntry = manager.getLogEntry(revision);
                if (logEntry == null) {
                    String msg = "No commit message for revision " + revision;
                    log.debug(msg);
                    warnings.add(msg);
                } else {
                    if (revision > 0) {
                        // Filter out messages used as part of indicating a release
                        if (releaseDetector == null || !releaseDetector.isMarker(logEntry.getMessage())) {
                            String msg = "No JIRA issue key in r" + revision + ": " + logEntry.getMessage();
                            log.debug(msg);
                            warnings.add(msg);
                        }
                    }
                }
            } else {
                log.debug("revision " + revision + " refers to issue(s): " + issueKeys);
                for (String issueKey: issueKeys) {
                    String projectKey = JiraKeyUtils.getFastProjectKeyFromIssueKey(issueKey.toUpperCase());
                    if (projectKey == null || projectManager.getProjectObjByKey(projectKey) == null) {
                        String msg = "Falsely identified "  + issueKey + " as an issue in revision " + revision;
                        log.info(msg);
                        continue;
                    }

                    Issue issue = issueManager.getIssueObject(issueKey);
                    if (issue == null) {
                        String msg = "Unknown issue " + issueKey + " found in revision " + revision;
                        log.warn(msg);
                        warnings.add(msg);
                        continue;
                    }

                    allIssueKeys.add(issueKey);
                }
            }
        }
            
        return allIssueKeys;
    }

    /**
     * Get the start value changeset
     */
    protected long getStartValue(ErrorCollection errors, 
                                 List<String> warnings, 
                                 String startChangeSet, 
                                 ReleaseDetector detector,
                                 MercurialManager manager,
                                 RevisionIndexer indexer,
                                 Map otherParams) {
        long start = -1;
        if (startChangeSet.equals(PREVIOUS)) {
            if (detector == null) {
                errors.addErrorMessage("start: numeric changeset identifiers are all that is currently enabled");
                return -1;
            }
            start = detector.getPreviousRelease(errors);
            if (errors.hasAnyErrors()) {
                return -1;
            }
            String prevVersionName = detector.getVersion(start); // TODO not yet used
            otherParams.put("start", new Long(start));
            return start;
        }

        // TODO add support for infering the version name from the changesets
        try {
            start = Long.parseLong(startChangeSet);
            otherParams.put("start", new Long(start));
        } catch (NumberFormatException nfe) {
            // Assumes that no hg tags are simply digits
            try {
                start = indexer.getRevisionForTag(manager, startChangeSet);
            } catch (IOException e) {
                log.warn("Unable to get revision for tag from repository '" + manager.getDisplayName() + "'", e);
            } catch (IndexException e) {
                log.warn("Unable to get revision for tag from repository '" + manager.getDisplayName() + "'", e);
            }

            if (start != -1) {
                otherParams.put("start", new Long(start));
                // Display the start tag as part of the "versionName" variable
                otherParams.put("versionName", startChangeSet);
            } else {
                String msg = "No number or valid tag found " + nfe.getMessage();
                log.warn(msg);
                errors.addErrorMessage(msg);
                return -1;
            }
        }
        return start;
    }

    /**
     * Get the end value changeset
     *
     * Note: side effect: sets versionName in otherParams
     */
    protected long getEndValue(ErrorCollection errors, 
                               List<String> warnings, 
                               String endChangeSet, 
                               ReleaseDetector detector,
                               MercurialManager manager,
                               RevisionIndexer indexer,
                               Map otherParams,
                               long latest) {
        long end = -1;
        if (latest == -1) {
            String msg = "There was a problem obtaining the latest revision from the repository " + manager.getDisplayName();
            //log.warn(msg); // logged earlier
            errors.addErrorMessage(msg);
            return -1;
        }
        if (endChangeSet.equals(LATEST)) {
            if (detector == null) {
                errors.addErrorMessage("end: numeric changeset identifiers are all that is currently enabled");
                return -1;
            }
            // TODO how to offer the simple case: end = latest;
            end = detector.getLatestRelease(errors);
            if (errors.hasAnyErrors()) {
                return -1;
            }
            // This is where the last version that email was sent about is
            // recorded for checking when !isManual
            otherParams.put("lastVersionName", manager.getReleasenoteslatest());

            otherParams.put("versionName", detector.getVersion(end));
            otherParams.put("end", new Long(end));
            return end;
        }

        try {
            end = Long.parseLong(endChangeSet);
            otherParams.put("end", new Long(end));
        } catch (NumberFormatException nfe) {
            // Assumes that no hg tags are simply digits
            try {
                end = indexer.getRevisionForTag(manager, endChangeSet);
            } catch (IOException e) {
                log.warn("Unable to get revision for tag from repository '" + manager.getDisplayName() + "'", e);
            } catch (IndexException e) {
                log.warn("Unable to get revision for tag from repository '" + manager.getDisplayName() + "'", e);
            }

            if (end != -1) {
                otherParams.put("end", new Long(end));
                // Display the end tag as part of the "versionName" variable
                String startVersionName = (String)otherParams.get("versionName");
                if (startVersionName != null) {
                    startVersionName = startVersionName + " : " + endChangeSet;
                    otherParams.put("versionName", startVersionName);
                } else {
                    // The start parameter could have been a number
                    otherParams.put("versionName", endChangeSet);
                }
            } else {
                String msg = "No number or valid tag found " + nfe.getMessage();
                log.warn(msg);
                errors.addErrorMessage(msg);
                return -1;
            }
        }
        if (end > latest) {
            String msg = "Revision " + end + " is greater than the latest revision " + latest + ". Using the latest revision instead.";
            log.warn(msg);
            warnings.add(msg);
            otherParams.put("end", new Long(end));
            end = latest;
        }
        return end;
    }

    /**
     * Update the named text field with the versionName parameter in otherParams
     */
    protected void updateReleaseField(ErrorCollection errors, 
                                      List<String> warnings,
                                      String addressedInFieldName,
                                      Set<String> issueKeys,
                                      Map otherParams) {
        CustomField cf = customFieldManager.getCustomFieldObjectByName(addressedInFieldName);
        if (cf == null) {
            String msg = "Unable to find the field for release information: " + addressedInFieldName;
            warnings.add(msg);
            log.warn(msg);
            return;
        }

        String cft_key = cf.getCustomFieldType().getKey();
        if (!cft_key.endsWith(":textfield") && 
            !cft_key.endsWith(":textarea") && 
            !cft_key.endsWith(":readonlyfield") && 
            !cft_key.endsWith(":readonlytextarea") && 
            !cft_key.endsWith(":url")) {
            String msg = "The field '" + addressedInFieldName + "' is not a text field";
            warnings.add(msg);
            log.warn(msg);
            return;
        }

        // This is put in getEndValue only if end is LATEST and the detector
        // was able to find a value for the latest release
        String versionName = (String)otherParams.get("versionName");
        if (versionName == null) {
            String msg = "Unable to automatically find a release label so not updating " + addressedInFieldName;
            warnings.add(msg);
            log.warn(msg);
        }

        if (versionName.length() > 254) {
            // Truncating silently could be unexpected, better to fail cleanly
            String msg = "Release label " + versionName + " is too long (>254 characters)";
            warnings.add(msg);
            log.warn(msg);
            return;
        }

        log.debug("Updating the field " + addressedInFieldName + " to " + versionName + " in " + issueKeys.size() + " issue(s)");

        for (String issueKey: issueKeys) {
            Issue issue = issueManager.getIssueObject(issueKey);
            if (issue == null) {
                // This is unlikely to occur, should be filtered earlier
                String msg = "Cannot update the unknown issue " + issueKey;
                log.warn(msg);
                warnings.add(msg);
                continue;
            }
            // TODO what if the field is not valid for that issue?
            cf.createValue(issue, versionName);
            issue.store();
        }
    }

    /**
     * Return the HTML generated from a velocity template containing
     * the information in the given issueKeys.
     *
     * Populate warnings as necessary.
     */
    protected String getReport(ErrorCollection errors, 
                               List<String> warnings,
                               User user, 
                               Set<String> issueKeys, 
                               String repoName,
                               String titlePrefix,
                               Map otherParams) {
        Map velocityParams = new HashMap();
        velocityParams.put("ctx", velocityRequestContext);

        if (user != null) {
            velocityParams.put("i18n", new I18nBean(user));
        } else {
            // Use the default locale
            velocityParams.put("i18n", new I18nBean());
        }
        velocityParams.put("baseurl", velocityRequestContext.getCanonicalBaseUrl());

        velocityParams.put("warnings", warnings);

        // sections are one for each issue type
        Map<String, Map<String, Map<String, String>>> sections = new HashMap<String, Map<String, Map<String, String>>>();

        for (String issueKey: issueKeys) {
            Issue issue = issueManager.getIssueObject(issueKey);
            if (issue == null) {
                // In production, this should have been filtered out
                // earlier than this. During development an issue may
                // not exist.
                String msg = "Unknown issue " + issueKey;
                log.warn(msg);
                warnings.add(msg);
                continue;
            }
            String issuetype = issue.getIssueTypeObject().getName();
            Map rows = sections.get(issuetype);
            if (rows == null) {
                rows = new TreeMap();
                sections.put(issuetype, rows);
            }

            String status = issue.getStatusObject().getName();
            String priority = issue.getPriority().getString("name");
            String key = issue.getKey();
            Map<String, String> row = new HashMap<String, String>();
            row.put("summary", issue.getSummary());
            row.put("key", key);
            row.put("status", status);
            row.put("priority", priority);
            
            // Can be "" or a deleted user or a current user
            String lastChangedById = getLastChangedBy(issue);
            String lastChangedBy = lastChangedById;
            User lastChangedByUser = userUtil.getUser(lastChangedBy);
            if (lastChangedByUser != null) {
                lastChangedBy = lastChangedByUser.getDisplayName();
            }
            row.put("lastchangedby", lastChangedBy);

            Set<String> committerNames = getCommitters(issueKey);
            String committers = null;
            if (!committerNames.isEmpty()) {
                if (committerNames.size() == 1 && committerNames.iterator().next().equals(lastChangedById)) {
                    // Don't show unnecessary information
                    committers = null;
               } else {
                    StringBuffer sb = new StringBuffer();
                    for (Iterator it = committerNames.iterator(); it.hasNext(); ) {
                        String name = (String)it.next();
                        sb.append(name.trim());
                        if (it.hasNext()) {
                            sb.append(", ");
                        }
                    }
                    committers = sb.toString();
                }
            }
            row.put("committers", committers);

                    String rowKey = status+priority+key; // order by status then priority then key
            log.debug("Adding report key: " + rowKey + "=" + row);
            rows.put(rowKey, row);
        }
        velocityParams.put("sections", sections);

        Date createdDate = new Date();
        String created = ManagerFactory.getOutlookDateManager().getOutlookDate(applicationProperties.getDefaultLocale()).formatDateTimePicker(createdDate);
        velocityParams.put("created", created);
        velocityParams.put("reponame", repoName);
        velocityParams.put("titleprefix", titlePrefix);
        velocityParams.put("otherParams", otherParams);

        return renderTemplate("releasenotes.vm", velocityParams);
    }

    protected String renderTemplate(String template, Map velocityParams)
    {
        String TEMPLATE_DIRECTORY_PATH = "templates/plugins/mercurial/reports/";
        try
        {
            return velocityManager.getEncodedBody(TEMPLATE_DIRECTORY_PATH, template, applicationProperties.getEncoding(), velocityParams);
        }
        catch (VelocityException e)
        {
            log.warn("Error occurred while rendering velocity template for '" + TEMPLATE_DIRECTORY_PATH + "/" + template + "'.", e);
        }

        return "";
    }

    /**
     * The userid that last changed the status of the given issue
     */
    protected String getLastChangedBy(Issue issue) {
        final List<GenericValue> changeItemsForFieldGVs = ofBizDelegator.findByAnd("ChangeGroupChangeItemView", EasyMap.build("issue", issue.getId(), "field", "status"), EasyList.build("created DESC"));
        if (changeItemsForFieldGVs.isEmpty()) {
            return ""; // never changed status
        }
        return changeItemsForFieldGVs.get(0).getString("author");
    }

    /*
     * Get all the Mercurial user ids that made commits referring to
     * the given issue
     */
    protected Set<String> getCommitters(String issueKey) {
        RevisionIndexer indexer = multipleMercurialRepositoryManager.getRevisionIndexer();
        Set<String> authors = new HashSet();
        try {
            authors = indexer.getAuthorsByIssue(issueKey);
        } catch (Exception ex) {
            log.warn("Unable to find the authors in a commit message that refers to issue: " + issueKey + "message: " + ex.getMessage());
        }
        return authors;
    }
}
