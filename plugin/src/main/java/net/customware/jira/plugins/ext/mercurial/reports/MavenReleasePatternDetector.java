package net.customware.jira.plugins.ext.mercurial.reports;

import net.customware.jira.plugins.ext.mercurial.*; 
import net.customware.jira.plugins.ext.mercurial.revisions.*;

import com.atlassian.jira.util.ErrorCollection;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.regex.*;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * Detect a release in a group of changesets.
 * 
 */
public class MavenReleasePatternDetector implements ReleaseDetector {

    private static final Log log = LogFactory.getLog(RevisionIndexer.class);

    private String mavenStr = "\\[maven-release-plugin\\][\\s]+";

    private MercurialManager manager;
    private String latestArtifactId;

    /**
     * The release names keyed and ordered by changeset
     */
    private TreeMap<Long, String> releases; 

    public MavenReleasePatternDetector(MercurialManager manager, RevisionIndexer indexer, String artifactId) {
        this.manager = manager;

        // TODO cache these instances and then append to them?
        releases = new TreeMap<Long, String>();
        Map<Long, String> messageMap = indexer.getMessagesByRepo(manager.getId());
        if (messageMap == null) {
            log.warn("Unable to get changeset messages");
            return;
        }
        
        populateReleases(releases, messageMap, artifactId);
    }

    public long getLatestRelease(ErrorCollection errors) {
        log.debug("Found " + releases.size() + " release(s)");
        if (releases.isEmpty()) {
            String msg = "No latest release could be automatically detected in the repository " + manager.getDisplayName() + ". The JIRA index for the repository may not have been created yet.";
            log.warn(msg);
            errors.addErrorMessage(msg);
            return -1;
        }
        return releases.lastKey().longValue();
    }

    /**
     */
    public long getPreviousRelease(ErrorCollection errors) {
        log.debug("Found " + releases.size() + " release(s)");
        if (releases.size() < 2) {
            // TODO i18n
            String msg = "No previous release could be automatically detected in the change log for the repository '" + manager.getDisplayName() + "'";
            log.warn(msg);
            errors.addErrorMessage(msg);
            return -1;
        }
        // TODO ther must be a better way to get the second last element
        TreeMap<Long, String> releasesTmp = new TreeMap<Long, String>(releases);
        releasesTmp.remove(releases.lastKey());
        return releasesTmp.lastKey().longValue();
    }

    public String getVersion(long changeset) {
        return releases.get(new Long(changeset));
    }
    
    public boolean isMarker(String msg) {
        // All the patterns to suppress warnings about their commits
        // not having JIRA issue keys in the text
        String[] patterns = new String[] {mavenStr, "Automated merge with"};
        for (String p: patterns) {
            Pattern pattern = Pattern.compile(p);
            Matcher m = pattern.matcher(msg);
            if (m.find()) {
                return true;
            }
        }
        return false;
    }
    
    public String getLatestArtifactId() {
        return latestArtifactId;
    }

// Example of pattern:
// 
// changeset:   76:ca638f9eb787
// user:        releng <releng@example.com>
// date:        Fri Feb 12 14:14:29 2010 -0500
// summary:     [maven-release-plugin] prepare for next development iteration
// 
// changeset:   75:57c30b385248
// user:        releng <releng@example.com>
// date:        Fri Feb 12 14:14:27 2010 -0500
// summary:     [maven-release-plugin]  copy for tag apache-2.2.14.1
// 
// changeset:   74:ade60398a4be
// tag:         apache-2.2.14.1
// user:        releng <releng@example.com>
// date:        Fri Feb 12 14:14:26 2010 -0500
// summary:     [maven-release-plugin] prepare release apache-2.2.14.1

// The above is without -v. With -v, replace the summary line by:
// description:
// [maven-release-plugin] prepare release acs-1.0.87

    protected void populateReleases(Map<Long, String> releases, 
                                    Map<Long, String> messageMap,
                                    String artifactId) {
        Pattern step3Pattern = Pattern.compile(mavenStr + "prepare for next development iteration[\\s]*$", Pattern.MULTILINE);
        Pattern step2Pattern;
        Pattern step1Pattern;
        
        // TODO does the artifact always have the same syntax?
        if (artifactId.equals(ReportsResource.DEDUCE)) {
            // Patterns to extract the artifact id
            latestArtifactId = null;
            String artifactPattern = "([\\w]+)";
            step2Pattern = Pattern.compile(mavenStr + "copy for tag " + artifactPattern + "-([\\d].*)[\\s]*$", Pattern.MULTILINE);
            step1Pattern = Pattern.compile(mavenStr + "prepare release " + artifactPattern + "-([\\d].*)[\\s]*$", Pattern.MULTILINE);
        } else {
            // Patterns using a given artifact id
            latestArtifactId = artifactId;
            step2Pattern = Pattern.compile(mavenStr + "copy for tag " + artifactId + "-([\\d].*)[\\s]*$", Pattern.MULTILINE);
            step1Pattern = Pattern.compile(mavenStr + "prepare release " + artifactId + "-([\\d].*)[\\s]*$", Pattern.MULTILINE);
        }

        // messageMap is ordered descending by changeset
        int step = 0;
        String version = "";
        String currentArtifactId = "";
        for (Iterator it = messageMap.entrySet().iterator(); it.hasNext(); ) {
            Map.Entry entry = (Map.Entry)it.next();
            Long cs = (Long)entry.getKey();
            String message = (String)entry.getValue();

            // Find commits that match each step in turn
            Matcher m;
            log.debug("Checking step " + step + " for changeset " + cs + " and " + " artifact " + artifactId + " and message " + message);
            if (step == 0) {
                m = step3Pattern.matcher(message); // TODO multiline?
                if (m.matches()) {
                    step++;
                } else {
                    step = 0;
                }
            } else if (step == 1) {
                m = step2Pattern.matcher(message);
                if (m.matches()) {
                    step++;
                    if (latestArtifactId == null) {
                        currentArtifactId = m.group(1);
                        version = m.group(2);
                    } else {
                        currentArtifactId = latestArtifactId; // specified
                        version = m.group(1);
                    }
                    log.debug("Found artifact " + artifactId + " and version " + version);
                } else {
                    step = 0;
                }
            } else if (step == 2) {
                m = step1Pattern.matcher(message);
                if (m.matches()) {
                    String currentVersion;

                    if (latestArtifactId == null) {
                        currentVersion = m.group(2);
                        String currentArtifactId2 = m.group(1);
                        if (!currentArtifactId.equals(currentArtifactId2)) {
                            // Two releases were run concurrently?
                            log.warn("Artifact changed from " + currentArtifactId + " to " + currentArtifactId2 + " at changeset " + cs);
                            continue;
                        }
                    } else {
                        currentVersion = m.group(1);
                    }
                    
                    log.debug("Current version pattern is |" + currentVersion + "|");
                    log.debug("Previous version pattern is |" + version + "|");
                    if (version.equals(currentVersion)) {
                        latestArtifactId = currentArtifactId;
                        String releaseName = latestArtifactId + "-" + version;
                        releases.put(cs, releaseName);
                        log.debug("Found release " + releaseName + " for artifact " + latestArtifactId + " at changeset " + cs);
                    }
                }
                step = 0;
                version = "";
            } else {
                log.warn("Unexpected step reached " + step);
            }
            log.debug("Reached step " + step + " with changeset " + cs + " and message:" + message);

        }
    }
    
}
